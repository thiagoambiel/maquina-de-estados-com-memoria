# Maquina de Estados com Memoria

**Simulador de uma Maquina de Estados com Memoria RAM implementado em Python.**

Este repositório contém um notebook comentado com uma representação em código de como uma **Máquina de Estados**, junto de uma **Memória (RAM)** funcionam na prática.

# Algoritmo

O algoritmo implementado através da Maquina de Estados simula um sistema de alarme de carro, onde o **Botão (b)** e o **Sensor da Porta (sp)** são entradas no sistema, e a **Buzina (bz)** e a **Luz Vermelha (lv)** são as saídas.

O sistema como um todo possui a seguinte **Tabela Verdade**:

Nota: **E1** e **E0** são os bits 0 e 1, respectivamente, da variável **Estado** na Máquina de Estados.

| E1 | E0 | B | SP | E1' | E0' | BZ | LV |
|----|----|---|----|-----|-----|----|----|
| 0  | 0  | 0 | 0  | 0   | 0   | 0  | 0  |
| 0  | 0  | 0 | 1  | 0   | 0   | 0  | 0  |
| 0  | 0  | 1 | 0  | 0   | 1   | 0  | 1  |
| 0  | 0  | 1 | 1  | 0   | 1   | 0  | 1  |
| 0  | 1  | 0 | 0  | 0   | 1   | 0  | 1  |
| 0  | 1  | 0 | 1  | 1   | 0   | 1  | 1  |
| 0  | 1  | 1 | 0  | 0   | 0   | 0  | 0  |
| 0  | 1  | 1 | 1  | 0   | 0   | 0  | 0  |
| 1  | 0  | 0 | 0  | 1   | 0   | 1  | 1  |
| 1  | 0  | 0 | 1  | 1   | 0   | 1  | 1  |
| 1  | 0  | 1 | 0  | 0   | 0   | 0  | 0  |
| 1  | 0  | 1 | 1  | 0   | 0   | 0  | 0  |

## License
Este repositório esta sob a licença do MIT e pode ser usado e copiado por qualquer um.
